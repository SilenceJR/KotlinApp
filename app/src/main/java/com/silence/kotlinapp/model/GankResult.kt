package com.silence.kotlinapp.model

data class GankResult<out T>(val error: Boolean, val results: T)