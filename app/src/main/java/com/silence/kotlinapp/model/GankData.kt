package com.silence.kotlinapp.model

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/13 / 14:28
 *
 */
data class GankData(val _id: String, val createdAt: String, val desc: String, val images: List<String>?,
                    val publishedAt: String, val source: String, val type: String, val url: String,
                    val used: String, val who: String)

/**

{
"_id": "5b1f6690421aa94dd38ff2b5",
"createdAt": "2018-06-12T14:22:08.329Z",
"desc": "Android \u4eff\u638c\u9605\u4e66\u67b6",
"images": [
"http://img.gank.io/dd0a6367-425a-4d6e-a470-6971da217568",
"http://img.gank.io/da9843d2-de11-4b02-861f-72c209212e40"
],
"publishedAt": "2018-06-13T00:00:00.0Z",
"source": "web",
"type": "Android",
"url": "https://github.com/AlphaBoom/ClassifyView",
"used": true,
"who": "joker"
}

        **/