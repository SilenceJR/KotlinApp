package com.silence.kotlinapp.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/26 / 14:36
 *
 */
class MainViewPagerAdapter: FragmentPagerAdapter {


    private val mTitlts = arrayOf("Android", "ios", "福利")
    private var mFragments: List<Fragment>?

    constructor(fm: FragmentManager?, mFragments: List<Fragment>?) : super(fm) {
        this.mFragments = mFragments
    }

    override fun getCount(): Int {
        return mFragments!!.size
    }

    override fun getItem(position: Int): Fragment {
        return mFragments!![position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mTitlts[position]
    }
}