package com.silence.kotlinapp.adapter

import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.silence.kotlinapp.R
import com.silence.kotlinapp.model.GankData

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/13 / 15:57
 *
 */
class GankDefaultAdapter(data: List<GankData>?): BaseQuickAdapter<GankData, BaseViewHolder>(R.layout.item_gank_default, data) {

    override fun convert(helper: BaseViewHolder, item: GankData?) {
        helper.setText(R.id.tv_title, item?.desc)
                .setText(R.id.tv_content, item?.desc)

        if (null != item?.images && 0 != item.images.size) {
            Glide.with(mContext)
                    .load(item.images.get(0))
                    .into(helper.getView(R.id.iv_img))
        }

    }
}