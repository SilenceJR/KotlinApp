package com.silence.kotlinapp.adapter

import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.target.ImageViewTarget
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.silence.kotlinapp.R
import com.silence.kotlinapp.model.GankData

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/13 / 15:57
 *
 */
class GankWelfareAdapter(data: List<GankData>?): BaseQuickAdapter<GankData, BaseViewHolder>(R.layout.item_gank_welfare, data) {

    override fun convert(helper: BaseViewHolder, item: GankData?) {

        val tvInfo = helper.getView<TextView>(R.id.tv_info)
        val ivImg = helper.getView<ImageView>(R.id.iv_img)

        if (null != item?.url) {
            Glide.with(mContext)
                    .load(item.url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .into(object : ImageViewTarget<GlideDrawable>(ivImg) {
                        override fun setResource(resource: GlideDrawable) {
                            val width = resource.intrinsicWidth
                            val height = resource.intrinsicHeight
                            tvInfo.setText("$width * $height")

                            ivImg.setImageDrawable(resource)

                        }

                    })


        }

    }


}