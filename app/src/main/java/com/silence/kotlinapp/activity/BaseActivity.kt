package com.silence.kotlinapp.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import com.silence.kotlinapp.R
import com.silence.kotlinapp.mvp.view.IView
import com.silence.kotlinapp.util.Utils
import com.silence.kotlinapp.widget.BaseToolbar
import com.silence.kotlinapp.widget.ToolBarCommView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_base.*

abstract class BaseActivity : AppCompatActivity(), IView {

    private var mToolbar: BaseToolbar? = null
    private var mCompositeDisposable: CompositeDisposable = CompositeDisposable()
    protected var mToolbarView: ToolBarCommView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        if (isSupportTranslucentStatus()) {
            Utils.fitStatusBar(this, false)
        }
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_base)

        root_view.addView(LayoutInflater.from(this).inflate(getActivityContentView(), root_view, false), 1)

    }

    abstract fun getActivityContentView(): Int

    fun initToolbar(toolbarView: View): BaseToolbar? {
        mToolbarView = toolbarView as ToolBarCommView
        if (null == mToolbar) {
            mToolbar = vs_base.inflate() as BaseToolbar
        } else{
            mToolbar!!.removeAllViews()
        }

        mToolbar!!.setFitStatusBar(isSupportTranslucentStatus())
        mToolbar!!.addView(toolbarView)

        return mToolbar
    }

    fun setToolbarTitle(title: String) {
        mToolbarView!!.setTitle(title)
    }

    fun setToolbarTitle(resId: Int) {
        mToolbarView!!.setTitle(resId)
    }

    open fun isSupportTranslucentStatus(): Boolean {
        return true
    }

    override fun addDisposable(d: Disposable) {
        mCompositeDisposable.add(d)
    }

    override fun onDestroy() {
        mCompositeDisposable.clear()
        super.onDestroy()
    }

}
