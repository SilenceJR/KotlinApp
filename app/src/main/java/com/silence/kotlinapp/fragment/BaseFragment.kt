package com.silence.kotlinapp.fragment


import android.support.v4.app.Fragment
import com.silence.kotlinapp.mvp.view.IView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * A simple [Fragment] subclass.
 *
 */
abstract class BaseFragment : Fragment(), IView {

    private val mCompositeDisposable = CompositeDisposable()

    override fun addDisposable(d: Disposable) {
        mCompositeDisposable.add(d)
    }

    override fun onDestroy() {
        mCompositeDisposable.clear()
        super.onDestroy()
    }

}
