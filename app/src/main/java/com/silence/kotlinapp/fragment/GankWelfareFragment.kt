package com.silence.kotlinapp.fragment

import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chad.library.adapter.base.BaseQuickAdapter
import com.silence.kotlinapp.R
import com.silence.kotlinapp.adapter.GankWelfareAdapter
import com.silence.kotlinapp.model.GankData
import com.silence.kotlinapp.mvp.controller.GankWelfareController
import com.silence.kotlinapp.mvp.presenter.Impl.GankWelfarePresenterImpl
import kotlinx.android.synthetic.main.fragment_gank_default.*
import org.jetbrains.anko.support.v4.toast

class GankWelfareFragment : BaseFragment(), GankWelfareController.View, BaseQuickAdapter.RequestLoadMoreListener {

    companion object {
        @JvmStatic
        fun newInstance(): GankWelfareFragment {
            return GankWelfareFragment()
        }
    }

    private lateinit var mPresenter: GankWelfareController.Presenter
    private lateinit var mAdapter: GankWelfareAdapter
    var page = 1
    var flag = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mPresenter = GankWelfarePresenterImpl(this@GankWelfareFragment)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gank_default, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()

        initData()
    }

    private fun initData() {
        mPresenter.loadData(page)
    }

    private fun initView() {
        mAdapter = GankWelfareAdapter(null)
        mAdapter.openLoadAnimation()
        mAdapter.setEnableLoadMore(true)
        mAdapter.setOnLoadMoreListener(this, recycler)

        recycler.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        recycler.adapter = mAdapter

    }

    override fun onLoadMoreRequested() {
        initData()
    }



    override fun onError(msg: String, code: Int) {
        toast(msg)
        mAdapter.loadMoreFail()
    }

    override fun onNetFinish() {
        if (mAdapter.isLoading) {
            mAdapter.loadMoreComplete()
        }

        if (!mAdapter.isLoadMoreEnable) {
            mAdapter.setEnableLoadMore(true)
        }
    }


    override fun onLoadSuccess(t: List<GankData>) {
        page++
        if (flag) {
            mAdapter.setNewData(t)
            flag = false
        } else {
            mAdapter.addData(t)
        }
    }

}
