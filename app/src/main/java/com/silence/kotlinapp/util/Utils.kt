package com.silence.kotlinapp.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager


/**
 *
 *  @作者:  PJ
 *  @创建时间:    2018/6/7 / 15:46
 *  @描述:  这是一个 Utils 类.
 *
 */
class Utils {

    companion object {

        /**
         * 透明状态栏
         *
         * @param activity
         * @param statusBarShowBlackTitle 状态栏是否黑色字体
         */
        fun fitStatusBar(activity: Activity, statusBarShowBlackTitle: Boolean) {
            fitStatusBar(activity, statusBarShowBlackTitle, false, Color.TRANSPARENT)
        }

        /**
         * 透明状态栏
         *
         * @param activity
         * @param statusBarNightTxt 状态栏是否黑色字体
         */
        fun fitStatusBar(activity: Activity, statusBarNightTxt: Boolean, hideNavigationBar: Boolean, navigationBarColor: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                val window = activity.window
                val decorView = window.decorView
                //4.4 以上全屏 并隐藏底部导航栏  粘性沉浸
                var viewVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                if (hideNavigationBar) {//是否隐藏 底部导航
                    viewVisibility = (viewVisibility
                            or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION)
                }
                //状态栏  导航栏透明
                val attributes = window.attributes
                attributes.flags = (attributes.flags
                        or WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
                        or WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
                        or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
                    )
                    window.statusBarColor = Color.TRANSPARENT
                    if (navigationBarColor != 0) {
                        window.navigationBarColor = navigationBarColor//半透明
                    } else{
                        window.navigationBarColor = Color.TRANSPARENT
                    }
                    if (statusBarNightTxt) {
                        attributes.flags = attributes.flags or WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            //6.0 以上设置  状态栏是否暗色字符
                            viewVisibility = viewVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                        }
                    }
                }
                decorView.systemUiVisibility = viewVisibility
            }
        }

        /**
         * 获取状态栏高度
         *
         * @param context
         * @return
         */
        fun getStatusBarH(context: Context): Int {
            /**
             * 获取状态栏高度——方法1
             */
            var statusBarHeight: Int
            //获取status_bar_height资源的ID
            val resourceId = context.resources
                    .getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                //根据资源ID获取响应的尺寸值
                statusBarHeight = context.resources.getDimensionPixelSize(resourceId)
            } else {
                statusBarHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 25f, context
                        .resources
                        .displayMetrics).toInt()
            }
            return statusBarHeight
        }

        /**
         * 获取actionbar的高度
         *
         * @param context
         * @return
         */
        fun getActionBarSize(context: Context): Int {
            var actionBarHeight = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 45f, context
                    .resources
                    .displayMetrics).toInt()
            val tv = TypedValue()
            if (context.theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.resources
                        .displayMetrics)
            }
            return actionBarHeight
        }

        fun openKeyBorad(context: Context, view: View) {
            val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (!inputMethodManager.isActive()) {
                inputMethodManager.showSoftInputFromInputMethod(view.windowToken, InputMethodManager.RESULT_SHOWN)
            }
        }

        fun closeKeyBorad(context: Context, view: View) {
            val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (inputMethodManager.isActive()) {
                inputMethodManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            }

        }


    }


}