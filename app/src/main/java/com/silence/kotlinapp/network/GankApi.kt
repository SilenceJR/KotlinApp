package com.silence.kotlinapp.network

import com.silence.kotlinapp.model.GankData
import com.silence.kotlinapp.model.GankResult
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/12 / 18:46
 *
 */
interface GankApi {

    @GET("Android/{number}/{page}")
    fun Android(@Path("number") number: Int, @Path("page") page: Int): Observable<GankResult<List<GankData>>>

    @GET("iOS/{number}/{page}")
    fun ios(@Path("number") number: Int, @Path("page") page: Int): Observable<GankResult<List<GankData>>>

    @GET("福利/{number}/{page}")
    fun welfare(@Path("number") number: Int, @Path("page") page: Int): Observable<GankResult<List<GankData>>>

}