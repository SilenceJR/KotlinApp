package com.silence.kotlinapp.mvp.presenter

import com.silence.kotlinapp.mvp.presenter.Impl.IPresenter
import com.silence.kotlinapp.mvp.view.IView

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/26 / 16:44
 *
 */
interface GankPresenter<view: IView, T>:IPresenter<view> {
    fun loadData(t: T)
}