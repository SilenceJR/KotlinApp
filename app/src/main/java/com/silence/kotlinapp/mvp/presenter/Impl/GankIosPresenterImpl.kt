package com.silence.kotlinapp.mvp.presenter.Impl

import com.silence.kotlinapp.model.GankData
import com.silence.kotlinapp.model.GankResult
import com.silence.kotlinapp.mvp.controller.GankIosController
import com.silence.kotlinapp.mvp.presenter.BasePresenter
import com.silence.kotlinapp.network.HttpCall.Companion.load
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/21 / 15:18
 *
 */
class GankIosPresenterImpl : BasePresenter<GankIosController.View>, GankIosController.Presenter {
    private var mView: GankIosController.View

    constructor(view: GankIosController.View) : super(view) {
        mView = view
    }

    override fun loadData(t: Int) = load(mApi.ios(10, t), object : Observer<GankResult<List<GankData>>> {
        override fun onComplete() {
            mView.onNetFinish()
        }

        override fun onSubscribe(d: Disposable) {
            mView.addDisposable(d)
        }

        override fun onNext(t: GankResult<List<GankData>>) {
            if (!t.error) {
                mView.onLoadSuccess(t.results)
            }
        }

        override fun onError(e: Throwable) {
            mView.onError(e.message.toString(), 0x01)
            mView.onNetFinish()
        }

    })

}