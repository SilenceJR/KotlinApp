package com.silence.kotlinapp.mvp.presenter

import com.silence.kotlinapp.mvp.presenter.Impl.IPresenter
import com.silence.kotlinapp.mvp.view.IView
import com.silence.kotlinapp.network.GankApi
import com.silence.kotlinapp.network.HttpCall
import java.lang.ref.WeakReference

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/21 / 15:13
 *
 */
public open class BasePresenter<V: IView>:IPresenter<V> {

    private val mWeakReference: WeakReference<V>
    protected val mApi: GankApi


    constructor(view: V) {
        mWeakReference = WeakReference(view)
        mApi = HttpCall().getGankApi()
    }


    override fun getView(): V? {
        val v = mWeakReference.get()
        return v
    }

    override fun unAttach() {
        mWeakReference.clear()
    }





}