package com.silence.kotlinapp.mvp.presenter.Impl

import com.silence.kotlinapp.model.GankData
import com.silence.kotlinapp.model.GankResult
import com.silence.kotlinapp.mvp.controller.GankAndroidController
import com.silence.kotlinapp.mvp.presenter.BasePresenter
import com.silence.kotlinapp.network.HttpCall.Companion.load
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/21 / 15:18
 *
 */
class GankAndroidPresenterImpl : BasePresenter<GankAndroidController.View>, GankAndroidController.Presenter {

    var mView: GankAndroidController.View? = null

    constructor(view: GankAndroidController.View) : super(view) {
        mView = view
    }

    override fun loadData(page: Int) = load(mApi.Android(10, page), object : Observer<GankResult<List<GankData>>> {
        override fun onComplete() {
        }

        override fun onSubscribe(d: Disposable) {
            mView!!.addDisposable(d)
        }

        override fun onNext(t: GankResult<List<GankData>>) {
            if (!t.error) {
                mView!!.onLoadSuccess(t.results)
                mView!!.onNetFinish()
            } else {
                onError(IllegalArgumentException("数据有问题？"))
            }
        }

        override fun onError(e: Throwable) {
            mView!!.onError(e.message.toString(), 0x01)
            mView!!.onNetFinish()
        }

    })


}