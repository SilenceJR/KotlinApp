package com.silence.kotlinapp.mvp.view

import io.reactivex.disposables.Disposable

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/21 / 15:14
 *
 */
interface BaseView<T> {

    fun setPresenter(presenter: T)

    fun addDisposable(d: Disposable)

}