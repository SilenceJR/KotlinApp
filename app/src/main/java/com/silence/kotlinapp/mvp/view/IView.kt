package com.silence.kotlinapp.mvp.view

import io.reactivex.disposables.Disposable

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/25 / 15:12
 *
 */
interface IView {

    fun addDisposable(d: Disposable)

    fun onError(msg: String, code: Int)

    fun onNetFinish()

}