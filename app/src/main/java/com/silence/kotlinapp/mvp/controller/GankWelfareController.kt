package com.silence.kotlinapp.mvp.controller

import com.silence.kotlinapp.model.GankData
import com.silence.kotlinapp.mvp.presenter.GankPresenter
import com.silence.kotlinapp.mvp.view.GankView

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/21 / 15:16
 *
 */
interface GankWelfareController {

    interface View: GankView<List<GankData>> {
    }

    interface Presenter: GankPresenter<View, Int> {
    }

}