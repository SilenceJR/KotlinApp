package com.silence.kotlinapp.mvp.presenter.Impl

import com.silence.kotlinapp.mvp.view.IView

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/25 / 15:24
 *
 */
interface IPresenter<V: IView> {

    fun getView():V?

    fun unAttach()

}