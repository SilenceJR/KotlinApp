package com.silence.kotlinapp.mvp.view

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/26 / 16:46
 *
 */
interface GankView<T>: IView {
    fun onLoadSuccess(t: T)
}