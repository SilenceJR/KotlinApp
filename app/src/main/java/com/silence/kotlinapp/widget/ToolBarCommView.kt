package com.silence.kotlinapp.widget

import android.app.Activity
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.silence.kotlinapp.R
import kotlinx.android.synthetic.main.toolbar_comm.view.*

/**
 *
 *  @author:  PJ
 *  @time:    2018/6/21 / 16:24
 *
 */
class ToolBarCommView : RelativeLayout {

    val mToolbarBack: ImageView
    val mToolbarTitle: TextView
    val mToolbarSubtitle: TextView
    val mToolbarRightIcon: ImageView


    constructor(context: Context?) : this(context, null)
    constructor(context: Context?, attrs: AttributeSet?) : this(context, attrs, 0)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        View.inflate(context, R.layout.toolbar_comm, this)

        mToolbarBack = toolbar_back
        mToolbarTitle = toolbar_title
        mToolbarSubtitle = toolbar_subtitle
        mToolbarRightIcon = toolbar_right_icon

    }

    fun onBack() {
        val context = context
        if (context is Activity) context.onBackPressed()
    }

    fun setTitle(redId: Int): ToolBarCommView {
        mToolbarTitle.setText(redId)
        return this
    }

    fun setTitle(title: String): ToolBarCommView {
        mToolbarTitle.setText(title)
        return this
    }

    fun setSubTitle(redId: Int): ToolBarCommView {
        mToolbarSubtitle.setText(redId)
        return this
    }

    fun setSubTitle(title: String): ToolBarCommView {
        mToolbarSubtitle.setText(title)
        return this
    }
}